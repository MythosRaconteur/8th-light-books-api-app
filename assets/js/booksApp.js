const API_KEY = 'AIzaSyAIEloKDWFfhvm7Qf631dPgCCAfTpzAIWg';
const API_BASE_URL = 'https://www.googleapis.com/books/v1/volumes?q=';
const BATCH_SIZE = 20;


function bookSearch() {
    const str = $('#searchString').val();
    console.log(`Searching for ${str}...`);
    
    $('#resultsContainer').empty()
    
    $.ajax({
        url: API_BASE_URL + str,
        dataType: 'json',
        type: 'GET',
        data: {
            key: API_KEY,
            printType: 'books',
            maxResults: BATCH_SIZE
        },
        success: (data) => {
            if (data.totalItems > 0) {
                displayResultInfo(data.totalItems);
                
                data.items.forEach((book) => {
                    displayBook(book)
                })
            }
            else {
                $('#resultsContainer').append($('<h1>No Results Found...</h1>'))
            }
        }
    });
}

function displayResultInfo(numItems) {
    $.get('resultInfoPartial.mst', function(template) {
        const resultData = {
            queryString: $('#searchString').val(),
            numResults: BATCH_SIZE,
            approxTotalResults: numItems
        }
        
        const div = Mustache.render(template, resultData);
        $('#resultsContainer').append(div);
    });
}

function displayBook(bookData) {
    const templateData = {
        bookTitle: bookData.volumeInfo.title,
        bookImageURL: bookData.volumeInfo.imageLinks.thumbnail,
        bookDetailLink: bookData.volumeInfo.infoLink,
        bookAuthor: bookData.volumeInfo.authors ? bookData.volumeInfo.authors.join(", ") : "N/A",
        bookPublisher: bookData.volumeInfo.publisher,
        bookPublishDate: moment(new Date(bookData.volumeInfo.publishedDate)).format('LL'),
        bookDescription: bookData.volumeInfo.description
    }
    
    $.get('bookRowPartial.mst', function(template) {
        const row = Mustache.render(template, templateData);
        $('#resultsContainer').append(row);
    });
}


$(document).ready(() => {
    $('#searchButton').click(bookSearch);
    
    $('#searchString').keyup((evt) => {
        evt.preventDefault();
        if (evt.keyCode == 13) {
            bookSearch();
        }
    });
})